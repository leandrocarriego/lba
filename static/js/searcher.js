let searchTimeout;

const search = (inputElement) => {
  const searchQuery = inputElement.value;
  const searchIndicators = document.querySelectorAll(".js-search-indicator");
  const resultsContainers = document.querySelectorAll(
    ".js-search-results-container"
  );

  const toggleIndicators = () => {
    searchIndicators.forEach((indicator) => {
      indicator.classList.toggle("opacity-0");
    });
  };

  clearTimeout(searchTimeout);

  searchTimeout = setTimeout(() => {
    toggleIndicators();
    get(`/search/?q=${encodeURIComponent(searchQuery)}`)
      .then((response) => response.text())
      .then((response) => {
        toggleIndicators();
        resultsContainers.forEach((container) => {
          container.innerHTML = response;
        });
      })
      .catch((error) => {
        console.error("Request error: ", error);
      });
  }, 500);
};
