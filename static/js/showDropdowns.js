/*
Esta función toma un elemento padre y el evento asignado
si el evento es "mouseover" se muestra la lista hija "ul"
si el evento es "mouseout" se oculta la lista hija.
*/
function toggleDropdown(event, element) {
    const dropdown = element.querySelector("ul");

    if (dropdown) {
        if (event === "mouseover") {
            dropdown.classList.remove("hidden");
            dropdown.classList.add("block");
        } else if (event === "mouseout") {
            dropdown.classList.remove("block");
            dropdown.classList.add("hidden");
        }
    }
}

/*
Esta función toma un elemento padre y agrega los eventos
"mouseover" y "mouseout"
*/
function addEvents(element) {
    element.addEventListener("mouseover", () => {
        toggleDropdown("mouseover", element);
    });

    element.addEventListener("mouseout", () => {
        toggleDropdown("mouseout", element);
    });
}

/*
Esta función toma cada sección del navbar con la clase ".js-section"
las recorre y le agrega los eventos predeterminados
Luego hace lo mismo con todos sus dropdowns
*/
document.addEventListener("DOMContentLoaded", function () {
    const sections = document.querySelectorAll(".js-section");

    sections.forEach((section) => {
        addEvents(section);

        const dropdowns = section.querySelectorAll(".js-dropdown");

        dropdowns.forEach((dropdown) => {
            addEvents(dropdown);
        });
    });
});







