const acceptCookiesButton = document.getElementById("btn-accept-cookies");
const cookieNotice = document.getElementById("cookie-notice");
const cookieNoticeBg = document.getElementById("cookie-notice-bg");

dataLayer = [];

if (!localStorage.getItem("accepted-cookies")) {
  cookieNotice.classList.remove("hidden");
  cookieNoticeBg.classList.remove("hidden");
  cookieNotice.classList.add("block");
  cookieNoticeBg.classList.add("block");
} else {
  dataLayer.push({ event: "accepted-cookies" });
}

acceptCookiesButton.addEventListener("click", () => {
  cookieNotice.classList.remove("block");
  cookieNoticeBg.classList.remove("block");
  cookieNotice.classList.add("hidden");
  cookieNoticeBg.classList.add("hidden");

  localStorage.setItem("accepted-cookies", true);

  dataLayer.push({ event: "accepted-cookies" });
});