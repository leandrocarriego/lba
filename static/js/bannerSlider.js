document.addEventListener("DOMContentLoaded", function () {
  const images = document.querySelectorAll("#banner-carousel .banner-image");
  const imageCount = images.length;
  const interval =
    document.getElementById("banner-carousel").getAttribute("data-interval");
  let currentIndex = 0;

  function nextSlide() {
    const currentImage = images[currentIndex];
    const nextIndex = (currentIndex + 1) % imageCount;
    const nextImage = images[nextIndex];

    currentImage.classList.remove("translate-x-0");
    currentImage.classList.add("-translate-x-full");

    nextImage.classList.remove("-translate-x-full");
    nextImage.classList.add("translate-x-0");

    currentIndex = nextIndex;
  }

  if (imageCount > 1) {
    setInterval(nextSlide, interval);
  }
});
