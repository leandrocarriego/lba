document.addEventListener("DOMContentLoaded", () => {
  const articleUrl = document.getElementById('share-buttons-container').getAttribute('data-url');

  // Función para compartir en Facebook
  document.getElementById("shareFacebook").addEventListener("click", function () {
    window.open(
      "https://www.facebook.com/share.php?u=" + articleUrl,
      "Compartir en Facebook",
      "width=800,height=600"
    );
  });

  // Función para compartir en Twitter
  document.getElementById("shareTwitter").addEventListener("click", function () {
    window.open(
      "https://twitter.com/share?url=" + articleUrl,
      "Compartir en Twitter",
      "width=800,height=600"
    );
  });

  // Función para compartir en WhatsApp
  document.getElementById("shareWhatsApp").addEventListener("click", function () {
    window.open(
      "https://api.whatsapp.com/send?text=" + articleUrl,
      "Compartir en WhatsApp",
      "width=800,height=600"
    );
  });
});

