function sendQuiz(selectedOption) {
    const baseUrl = window.location.origin;
    const quizContainers = document.querySelectorAll('.quiz-container');
    const spinnerContainers = document.querySelectorAll('.quiz-spinner');
    const quizId = document.querySelector('.quiz-question').getAttribute("data-quiz-id");
    const selectedOptionData = selectedOption.getAttribute("data-option");

    spinnerContainers.forEach(spinnerContainer => {
      spinnerContainer.classList.remove('hidden');
    });

    const csrfToken = document.querySelector(
      "[name=csrfmiddlewaretoken]"
    ).value;

    fetch(`${baseUrl}/quizzes/api/quiz/${quizId}/option/${selectedOptionData}/`, {
      method: "POST",
      headers: {
        "X-CSRFToken": csrfToken,
      },
    })
      .then((response) => response.text())
      .then((response) => {
        setTimeout(() => {
          quizContainers.forEach(quizContainer => {
            quizContainer.innerHTML = response;
          })
          spinnerContainers.forEach(spinnerContainer => {
            spinnerContainer.classList.add('hidden');
          });
        }, 200);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }