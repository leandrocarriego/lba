const commentForm = document.getElementById('comment-form');
const commentFormHiddenElements = document.querySelectorAll('.js-comment-form-hidden-element');
const commentFormContentInput = document.querySelector('.comment-form-content-input');
const cancelCommentButton = document.getElementById('cancel-comment-button');

const openCommentForm = (e) => {
    commentFormHiddenElements.forEach(commentFormHiddenElement => {
        commentFormHiddenElement.classList.remove('hidden');
        commentFormContentInput.classList.remove('h-10');
        commentFormContentInput.classList.add('h-20');
    })
}

const closeCommentForm = (e) => {
    commentForm.reset();
    commentFormHiddenElements.forEach(commentFormHiddenElement => {
        commentFormHiddenElement.classList.add('hidden');
        commentFormContentInput.classList.remove('h-20');
        commentFormContentInput.classList.add('h-10');
    })
}


commentFormContentInput.addEventListener('click', openCommentForm);
cancelCommentButton.addEventListener('click', closeCommentForm);