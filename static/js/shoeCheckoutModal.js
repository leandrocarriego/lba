const modal = document.getElementById("modal");

// Abre el modal
function openModal() {
  modal.classList.remove("hidden");
}

// Cierra el modal
function closeModal() {
  modal.classList.add("hidden");
}
