addEventListener('DOMContentLoaded', () => {
    
    function updateCounter(counter, totalQuantity) {
        const currentValue = +counter.innerText;
        const increment = totalQuantity / 1000;

        if (currentValue < totalQuantity) {
            counter.innerText = Math.ceil(currentValue + increment);
            setTimeout(() => updateCounter(counter, totalQuantity), 40);
        } else {
            counter.innerText = totalQuantity;
        }
    }

    function animateCounters(entries) {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {
                setTimeout(() => increaseCounters(entry.target), 300);
            }
        });
    }

    function increaseCounters(quantityContainer) {
        const countersInElement = quantityContainer.querySelectorAll('.js-quantity');
        countersInElement.forEach((counter) => {
            const totalQuantity = +counter.dataset.totalQuantity;
            updateCounter(counter, totalQuantity);
        });
    }

    const counterObserver = new IntersectionObserver(animateCounters, {
        threshold: 0.75
    });

    const countersElements = document.getElementById('counters-container');
    counterObserver.observe(countersElements);
});
