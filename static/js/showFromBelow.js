addEventListener('DOMContentLoaded', () => {
    const showFromBellow = elements => {
        elements.forEach(element => {
            element.target.classList.add('opacity-0');
            if(element.isIntersecting) {
                element.target.classList.add('animate');
                element.target.classList.remove('opacity-0');
            }
        }
        )
    }
  
    const showObserver = new IntersectionObserver(showFromBellow, {
        threshold: 0.75
    })
  
    const elementsToShowFromBellow = document.querySelectorAll('.js-animate-show-below');
    elementsToShowFromBellow.forEach(element => {
        showObserver.observe(element);
    })
  })