document.addEventListener('DOMContentLoaded', function() {
    const baseUrl = window.location.origin;
    const likeButton = document.getElementById('like-button');
    const articleId = likeButton.getAttribute('data-article-id');
    let articleLikes = likeButton.getAttribute('data-article-likes');
    const likesCounter = document.getElementById('likes-counter');
    
    // Función para actualizar la apariencia del botón y el contador de "likes"
    const updateLikeButton = (likeButton, articleId, likesCounter, likes) => {
        const likedArticles = JSON.parse(localStorage.getItem('likedArticles')) || [];
        
        if (likedArticles.includes(articleId)) {
            // Pone el like
            document.getElementById('not-like-icon').classList.add('hidden');
            document.getElementById('like-icon').classList.remove('hidden');
        } else {
            // Saca el like
            document.getElementById('like-icon').classList.add('hidden');
            document.getElementById('not-like-icon').classList.remove('hidden');
        }
        // Actualiza el contador de "likes"
        likesCounter.innerHTML = likes;
    }

    // Carga el estado de "likes" al cargar la página
    updateLikeButton(likeButton, articleId, likesCounter, articleLikes);

    likeButton.addEventListener('click', function() {
        const csrfToken = document.querySelector('[name=csrfmiddlewaretoken]').value;
        let action;
        let likedArticles = JSON.parse(localStorage.getItem('likedArticles')) || [];

        // Verifica si el usuario ya le dio "like" a este artículo
        if (likedArticles.includes(articleId)) {
            // El usuario ya dio "like", así que lo quitamos
            likedArticles = likedArticles.filter(id => id !== articleId);
            action = 'remove';
        } else {
            // El usuario no ha dado "like", así que lo agregamos
            likedArticles.push(articleId);
            action = 'add';
        }
        
        fetch(`${baseUrl}/likes/api/article/${articleId}/${action}/`, {
            method: 'POST',
            headers: {
                'X-CSRFToken': csrfToken
            }
        })
        .then(response => response.json())
        .then(data => {
             // Guarda la lista de "likes" en el almacenamiento local
            localStorage.setItem('likedArticles', JSON.stringify(likedArticles));
            // Actualiza la apariencia del botón y el contador de "likes"
            updateLikeButton(likeButton, articleId, likesCounter, data.likes);
        })
        .catch(error => {
            console.error('Error:', error);
        });
    });      
});