document.addEventListener("DOMContentLoaded", () => {
    const input = document.querySelector("#phone");
    const iti = window.intlTelInput(input, {
      initialCountry: "auto",
      geoIpLookup: (callback) => {
        fetch("https://ipapi.co/json")
          .then((res) => res.json())
          .then((data) => callback(data.country_code))
          .catch(() => callback("us"));
      },
      utilsScript: "/intl-tel-input/js/utils.js?1695806485509",
    });
  
    document.querySelector('.iti--allow-dropdown').classList.add('w-full');
    
    document.getElementById("services-form").addEventListener("submit", (e) => {
      e.preventDefault();
  
      const spinnerContainer = document.getElementById('services-form-spinner');
      spinnerContainer.classList.remove('hidden');
  
      const messagesContainer = document.getElementById('messages-container');
      const sendFormUrl = document
        .getElementById("services-form")
        .getAttribute("data-url");
      const csrfToken = document.querySelector(
        "[name=csrfmiddlewaretoken]"
      ).value;
  
      const name = document.getElementById("name").value;
      const lastName = document.getElementById("last_name").value;
      const email = document.getElementById("email").value;
      const age = document.getElementById("age").value;
      const country = document.getElementById("country").value;
      const phone = iti.getNumber();
      const destination = document.getElementById("destination").value;
      const interests = document.getElementById("interests").value;
      const typeOfTravel = document.getElementById("type_of_travel").value;
      const numberOfTravelers = document.getElementById("number_of_travelers").value;
      const duration = document.getElementById("duration").value;
      const budget = document.getElementById("budget").value;
      const month = document.getElementById("month").value;
      const accommodation = document.getElementById("accommodation").value;
      const paceOfTravel = document.getElementById("pace_of_travel").value;
      const service = document.getElementById("service").value;
      const preferredContact = document.getElementById("preferred_contact").value;
      
      const message = document.getElementById("message").value;
      const recaptchaResponse = grecaptcha.getResponse();
  
      const formData = new FormData();
      formData.append("csrfmiddlewaretoken", csrfToken);
      formData.append("name", name);
      formData.append("last_name", lastName);
      formData.append("email", email);
      formData.append("age", age);
      formData.append("country", country);
      formData.append("phone", phone);

      formData.append("destination", destination);
      formData.append("interests", interests);
      formData.append("type_of_travel", typeOfTravel);
      formData.append("number_of_travelers", numberOfTravelers);
      formData.append("duration", duration);
      formData.append("budget", budget);
      formData.append("month", month);
      formData.append("accommodation", accommodation);
      formData.append("pace_of_travel", paceOfTravel);
      formData.append("service", service);
      formData.append("preferred_contact", preferredContact);
      
      formData.append("message", message);
      formData.append("g-recaptcha-response", recaptchaResponse);
  
      post(sendFormUrl, formData)
        .then((response) => response.json())
        .then((response) => {
          if (response.success) {
            e.target.reset();
            spinnerContainer.classList.add('hidden');
            grecaptcha.reset();
            messagesContainer.classList.remove('opacity-0', 'translate-y-full');
            setTimeout(() => {
              messagesContainer.classList.add('opacity-0', 'translate-y-full');
          }, 3000);
  
          } else {
            console.log(response);
          }
          
        })
        .catch((error) => {
          console.error("Request error: ", error);
        });
    });
  });