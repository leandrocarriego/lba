import os
from pathlib import Path
import environ

# Core configuration  --------------------------------------------------------------------------------------------------

env = environ.Env()
environ.Env.read_env()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = os.environ.get("SECRET_KEY")

DEBUG = True

# Allowed hosts
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS_PROD')

if DEBUG:
    ALLOWED_HOSTS = env.list('ALLOWED_HOSTS_DEV')

INTERNAL_IPS = [
    "127.0.0.1",
]

# Application definition -----------------------------------------------------------------------------------------------

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize'
]

PROJECT_APPS = [
    'apps.website',
    'apps.store',
]

THIRD_PARTY_APPS = [
    # 'ckeditor',
    # 'ckeditor_uploader',
    'rest_framework',
    # 'jazzmin',
]

if DEBUG:
    THIRD_PARTY_APPS += [
        # 'django_browser_reload',
        # 'debug_toolbar',
    ]

INSTALLED_APPS = PROJECT_APPS + THIRD_PARTY_APPS + DJANGO_APPS

# CKEditor -----------------------------------------------------------------------------------------------------------

# CKEDITOR_JQUERY_URL = 'https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js'
# CKEDITOR_UPLOAD_PATH = os.path.join(BASE_DIR, 'media/')
# CKEDITOR_IMAGE_BACKEND = 'pillow'

# CKEDITOR_CONFIGS = {
#     'default': {
#         'toolbar_Custom': [
#             {'name': 'document', 'items': ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
#             {'name': 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
#             {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll']},

#             '/',
#             {'name': 'basicstyles',
#              'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
#             {'name': 'paragraph',
#              'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-',
#                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl',
#                        'Language']},
#             {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
#             {'name': 'insert',
#              'items': ['Image', 'Youtube', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak']},
#             '/',
#             {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
#             {'name': 'colors', 'items': ['TextColor', 'BGColor']},
#             {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']},
#             {'name': 'about', 'items': ['CodeSnippet']},
#             {'name': 'about', 'items': ['About']},
#             '/',  # put this to force next toolbar on new line
#             {'name': 'yourcustomtools', 'items': [
#                 # put the name of your editor.ui.addButton here
#                 'Preview',
#                 'Maximize',

#             ]},
#         ],
#         'toolbar': 'Custom',  # put selected toolbar config here
#         'toolbarGroups': [{'name': 'document', 'groups': ['mode', 'document', 'doctools']}],
#         'height': 400,
#         # 'width': '100%',
#         'filebrowserWindowHeight': 725,
#         'filebrowserWindowWidth': 940,
#         'toolbarCanCollapse': True,
#         'mathJaxLib': '//cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML',
#         'tabSpaces': 4,
#         'extraAllowedContent': 'iframe[*];*(*){*}',
#         "removePlugins": ["stylesheetparser", "iframe"],
#         'extraPlugins': ','.join([
#             'uploadimage',
#             # your extra plugins here
#             'div',
#             'autolink',
#             'autoembed',
#             'embedsemantic',
#             'embedbase',
#             'embed',
#             'autogrow',
#             'devtools',
#             'widget',
#             'lineutils',
#             'clipboard',
#             'dialog',
#             'dialogui',
#             'elementspath',
#             'youtube',
#         ]),
#     }
# }

# Middlewares ----------------------------------------------------------------------------------------------------------

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
'''
if DEBUG:
    MIDDLEWARE += [
        'django_browser_reload.middleware.BrowserReloadMiddleware',
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]
'''

# Urls -----------------------------------------------------------------------------------------------------------------

ROOT_URLCONF = 'project.urls'

# Templates ------------------------------------------------------------------------------------------------------------

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Server WSGI ----------------------------------------------------------------------------------------------------------

WSGI_APPLICATION = 'project.wsgi.application'

# Database -------------------------------------------------------------------------------------------------------------

if DEBUG:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR / 'db.sqlite3'),
        }
    }

else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': os.environ.get("DB_NAME"),
            'USER': os.environ.get("DB_USER"),
            'PASSWORD': os.environ.get("DB_PASSWORD"),
            'HOST': os.environ.get("DB_HOST"),
            'PORT': os.environ.get("DB_PORT"),
        }
    }

# Password validation --------------------------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization -------------------------------------------------------------------------------------------------

LANGUAGE_CODE = 'es-AR'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

DATE_FORMAT = "d/m/Y"

# Static files (CSS, JavaScript, Images) -------------------------------------------------------------------------------

STATIC_URL = 'static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATICFILES_DIRS = [
    BASE_DIR / 'static_dev'
]

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# Default primary key field type ---------------------------------------------------------------------------------------

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Jazzmin config

# JAZZMIN_UI_TWEAKS = {
#     "theme": "flatly",
# }