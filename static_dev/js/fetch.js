async function get(url) {
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
    },
  });

  if (!response.ok) {
    const message = `Get request error: ${response.status}`;
    throw new Error(message);
  }

  return response;
}

async function post(url, data=false) {

  const response = await fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: data,
  });

  if (!response.ok) {
    const message = `Get request error: ${response.status}`;
    throw new Error(message);
  }

  return response;
}