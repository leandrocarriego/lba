document.addEventListener("DOMContentLoaded", () => {
  const input = document.querySelector("#phone");
  const iti = window.intlTelInput(input, {
    initialCountry: "auto",
    geoIpLookup: (callback) => {
      fetch("https://ipapi.co/json")
        .then((res) => res.json())
        .then((data) => callback(data.country_code))
        .catch(() => callback("us"));
    },
    utilsScript: "/intl-tel-input/js/utils.js?1695806485509",
  });

  document.querySelector('.iti--allow-dropdown').classList.add('w-full');
  
  document.getElementById("contact-form").addEventListener("submit", (e) => {
    e.preventDefault();

    const spinnerContainer = document.getElementById('contact-form-spinner');
    spinnerContainer.classList.remove('hidden');

    const messagesContainer = document.getElementById('messages-container');
    const addCommentUrl = document
      .getElementById("contact-form")
      .getAttribute("data-url");
    const csrfToken = document.querySelector(
      "[name=csrfmiddlewaretoken]"
    ).value;

    const name = document.getElementById("name").value;
    const lastName = document.getElementById("last_name").value;
    const phone = iti.getNumber();
    const country = document.getElementById("country").value;
    const email = document.getElementById("email").value;
    const subject = document.getElementById("subject").value;
    const message = document.getElementById("message").value;
    const recaptchaResponse = grecaptcha.getResponse();

    const formData = new FormData();
    formData.append("csrfmiddlewaretoken", csrfToken);
    formData.append("name", name);
    formData.append("last_name", lastName);
    formData.append("phone", phone);
    formData.append("country", country);
    formData.append("email", email);
    formData.append("subject", subject);
    formData.append("message", message);
    formData.append("g-recaptcha-response", recaptchaResponse);

    post(addCommentUrl, formData)
      .then((response) => response.json())
      .then((response) => {
        if (response.success) {
          e.target.reset();
          spinnerContainer.classList.add('hidden');
          grecaptcha.reset();
          messagesContainer.classList.remove('opacity-0', 'translate-y-full');
          setTimeout(() => {
            messagesContainer.classList.add('opacity-0', 'translate-y-full');
        }, 3000);

        } else {
          console.log(response);
        }
        
      })
      .catch((error) => {
        console.error("Request error: ", error);
      });
  });
});
