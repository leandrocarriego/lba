document.addEventListener("DOMContentLoaded", () => {

    document.getElementById('comment-form').addEventListener('submit', (e) => {
      e.preventDefault();
  
      const commentsContainer = document.getElementById('comments-container');
      const addCommentUrl = document.getElementById('comment-form').getAttribute("data-url");
      const csrfToken = document.querySelector('[name=csrfmiddlewaretoken]').value;
      const name = document.getElementById('comment-form-name-input').value;
      const email = document.getElementById('comment-form-email-input').value;
      const content = document.getElementById('comment-form-textarea').value;
  
      const formData = new FormData();
      formData.append('csrfmiddlewaretoken', csrfToken);
      formData.append('name', name);
      formData.append('email', email);
      formData.append('content', content);
  
      post(addCommentUrl, formData)
        .then((response) => response.text())
        .then((response) => {
          commentsContainer.insertAdjacentHTML('afterbegin', response);

          const newComment = commentsContainer.querySelector('.js-comment:first-child');

          newComment.classList.add('animate-comment'); 
          e.target.reset();
          
          setTimeout(() => {
            newComment.classList.remove('animate-comment');
        }, 3000);

        })
        .catch((error) => {
          console.error("Request error: ", error);
        });
  });
});
