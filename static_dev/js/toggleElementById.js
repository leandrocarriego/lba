function toggleElementById(id) {
    const elementToToggle = document.getElementById(id);

    if (elementToToggle) {
        elementToToggle.classList.toggle('hidden');
    }

}