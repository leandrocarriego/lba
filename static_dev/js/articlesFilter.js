const articlesContainer = document.getElementById('articles-list-container');
const filterLoadSpinner = document.getElementById('articles-filter-spinner');
let activeFilterButton = document.querySelector('.filter-active');

const filter = (selectedFilterButton) => {
    
    console.log(selectedFilterButton)

    if (selectedFilterButton.id != activeFilterButton.id) {
        filterLoadSpinner.classList.remove('hidden');
        articlesContainer.classList.add('hidden');
        activeFilterButton.classList.remove('filter-active');
        
        selectedFilterButton.classList.add('filter-active');
        activeFilterButton = selectedFilterButton;

        const filterUrl = selectedFilterButton.getAttribute('data-url') + `?${selectedFilterButton.getAttribute('data-q')}=${encodeURIComponent(selectedFilterButton.getAttribute('data-slug') || '')}`;

        fetch(filterUrl)
            .then(response => response.text())
            .then(data => {
                setTimeout(() => {
                    filterLoadSpinner.classList.add('hidden');
                    articlesContainer.innerHTML = data;
                    articlesContainer.classList.remove('hidden');
                   }, 200);
                
            })
            .catch(error => {
                console.error('Error loading content:', error);
            });
    }
};
   
    
    
    
    
    

