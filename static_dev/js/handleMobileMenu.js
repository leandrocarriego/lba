const handleMobileMenu = () => {
    const elementsToHandle = document.querySelectorAll('.js-handle-mobile-menu');

    elementsToHandle.forEach(element => {
        element.classList.toggle('hidden');
        element.scrollTop = 0;
    });
};