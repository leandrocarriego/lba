
    const navbar = document.getElementById('navbar');
    const menuCheckBox = document.getElementById('menu');
    const openMenuButton = document.getElementById('openMenuButton');
    const closeMenuButton = document.getElementById('closeMenuButton');
    const navbarLinksContainer = document.getElementById('navbarLinksContainer');

    window.addEventListener('scroll', () => {
      let scroll = window.scrollY;
    
      if (scroll > 50) {
        navbar.classList.add('bg-opacity-50');
      } else {
        navbar.classList.remove('bg-opacity-50');
      }
    });
