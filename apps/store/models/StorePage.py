from django.db.models import Model, CharField, ForeignKey, PROTECT

from apps.website.models import Banner


class StorePage(Model):
    class Meta:
        verbose_name = 'Página de la Tienda'
        verbose_name_plural = 'Página de la Tienda'

    top_subtitle = CharField(max_length=255, blank=True, null=True, verbose_name='Subtítulo superior')
    title = CharField(max_length=255, blank=True, null=True, verbose_name='Título')
    subtitle = CharField(max_length=255, blank=True, null=True, verbose_name='Subtítulo inferior')
    banner = ForeignKey(Banner, on_delete=PROTECT, blank=True, null=True)

    def __str__(self) -> str:
        return 'Tienda'