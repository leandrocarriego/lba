from django.db.models import Model, CharField, SlugField, IntegerField, ImageField


class Category(Model):
    name = CharField(max_length=100, verbose_name='Nombre')
    slug = SlugField(max_length=100, unique=True, verbose_name='Slug')
    position = IntegerField(verbose_name='Posición')
    thumbnail_image = ImageField(upload_to='categories/thumbnails/', verbose_name='Imagen en miniatura')

    class Meta:
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'
        ordering = ('position',)

    def __str__(self):
        return self.name
