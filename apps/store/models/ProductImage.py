from django.db.models import Model, IntegerField, ImageField, ForeignKey, CASCADE

from .Product import Product


class ProductImage(Model):
    product = ForeignKey(Product, related_name='images', on_delete=CASCADE, verbose_name='Producto')
    position = IntegerField(verbose_name='Posición')
    image = ImageField(upload_to='store/images/', verbose_name='Imagen')

    class Meta:
        verbose_name = 'Imagen de Producto'
        verbose_name_plural = 'Imágenes de Productos'

    def __str__(self):
        return f"Imagen de {self.product.name}"
