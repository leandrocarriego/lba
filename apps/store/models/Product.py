from django.db.models import Model, CharField, TextField, IntegerField, DecimalField, BooleanField, ImageField, ForeignKey, CASCADE

from .Category import Category


class Product(Model):
    name = CharField(max_length=100, verbose_name='Nombre')
    category = ForeignKey(Category, on_delete=CASCADE, related_name='product', verbose_name='Categoría')
    description = TextField(verbose_name='Descripción')
    highlight_1 = CharField(blank=True, null=True, max_length=100, verbose_name='Item destacado 1')
    highlight_2 = CharField(blank=True, null=True, max_length=100, verbose_name='Item destacado 2')
    highlight_3 = CharField(blank=True, null=True, max_length=100, verbose_name='Item destacado 3')
    highlight_4 = CharField(blank=True, null=True, max_length=100, verbose_name='Item destacado 4')
    details = TextField(blank=True, null=True, verbose_name='Detalles')
    price = DecimalField(max_digits=10, decimal_places=0, verbose_name='Precio')
    stock = IntegerField(default=1, verbose_name='Stock')
    is_unique = BooleanField(default=True, verbose_name='¿Es un producto único?')
    is_selected = BooleanField(default=False, verbose_name='¿Mostrar en productos seleccionados?')
    thumbnail_image = ImageField(upload_to='store/thumbnails/', verbose_name='Imagen en miniatura')

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        return self.name
