from django.views.generic import TemplateView, ListView, DetailView

from .models import Product, Category, ProductImage, StorePage


class ProductList(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'pages/store.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['page'] = StorePage.objects.first()
        context['categories'] = Category.objects.all()

        return context


class ProductDetail(DetailView):
    model = Product
    template_name = 'pages/product_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        product = self.get_object()

        context['product'] = product
        highlights = [
            product.highlight_1,
            product.highlight_2,
            product.highlight_3,
            product.highlight_4
        ]
        # Filtrar los campos que no estén vacíos (no sean None o cadenas vacías)
        filtered_highlights = [highlight for highlight in highlights if highlight]

        # Agregar al contexto solo si hay algún contenido
        if filtered_highlights:
            context['highlights'] = filtered_highlights

        context['images'] = ProductImage.objects.filter(product=product.id)

        return context


class CategoryList(ListView):
    model = Category
    context_object_name = 'categories'
    template_name = 'pages/categories.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['page'] = StorePage.objects.first()

        return context


class ProductByCategoryList(TemplateView):
    template_name = 'pages/store.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        category_slug = self.kwargs.get('category_slug')
        context['products'] = Product.objects.filter(category__slug=category_slug).select_related('category')
        context['categories'] = Category.objects.all()
        context['page'] = StorePage.objects.first()

        return context


class Checkout(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = 'pages/checkout.html'




'''class ProductDetail(RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductByCategoryList(ListAPIView):
    serializer_class = ProductSerializer

    def get_queryset(self):
        category_slug = self.kwargs['category_slug']
        category = get_object_or_404(Category, slug=category_slug)
        return Product.objects.filter(category=category)


class CategoryList(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer'''
