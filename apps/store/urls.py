from django.urls import path
from .views import *

app_name = 'store'

urlpatterns = [
    path('', ProductList.as_view(), name='store'),
    path('<int:pk>/', ProductDetail.as_view(), name='product-detail'),
    path('categories/', CategoryList.as_view(), name='category-list'),
    path('categories/<slug:category_slug>/', ProductByCategoryList.as_view(), name='product-by-category'),
    path('checkout/<int:pk>/', Checkout.as_view(), name='checkout'),
]