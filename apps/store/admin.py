from django.contrib import admin
from django.contrib.admin import ModelAdmin, StackedInline

from .models import Category, Product, ProductImage, StorePage


@admin.register(StorePage)
class StorePageAdmin(ModelAdmin):
    list_display = ('title', 'subtitle', 'banner')


@admin.register(Category)
class CategoryAdmin(ModelAdmin):
    list_display = ('name', 'position')
    prepopulated_fields = {'slug': ('name',), }
    ordering = ['position']


class ProductImageInline(StackedInline):
    model = ProductImage
    extra = 0


@admin.register(Product)
class ProductAdmin(ModelAdmin):
    list_display = ('name', 'price', 'category', 'stock', 'is_unique', 'is_selected')
    ordering = ['name', 'price', 'category__name']
    inlines = [ProductImageInline]
