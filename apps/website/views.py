from django.views.generic import TemplateView

from apps.website.models import HomePage, AboutPage
from apps.store.models.Product import Product


class HomeView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['page'] = HomePage.objects.select_related('banner').only('title', 'subtitle', 'banner').first()
        context['selected_products'] = Product.objects.filter(is_selected=True)

        return context


class AboutView(TemplateView):
    template_name = 'pages/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['page'] = AboutPage.objects.first()

        return context


class ContactView(TemplateView):
    template_name = 'pages/contact.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        #context['form'] = ContactForm()

        return context


