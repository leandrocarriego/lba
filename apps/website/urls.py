from django.urls import path
from .views import *

app_name = 'website'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('contact/', ContactView.as_view(), name='contact'),
    #path('cookies-policy/', CookiesView.as_view(), name='cookies_policy'),
]
