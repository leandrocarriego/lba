from django.contrib import admin
from django.contrib.admin import ModelAdmin, StackedInline

from apps.website.models import Banner, HomePage, AboutPage

@admin.register(Banner)
class BannerAdmin(ModelAdmin):
    list_display = ('name', 'interval')

@admin.register(HomePage)
class HomePageAdmin(ModelAdmin):
    list_display = ('title', 'top_subtitle', 'subtitle', 'banner')

@admin.register(AboutPage)
class AboutPageAdmin(ModelAdmin):
    list_display = ('title', 'subtitle', 'banner')

