from django.db.models import Model, CharField, TextField, IntegerField, URLField, ImageField, ForeignKey, \
    ManyToManyField, PROTECT
#from ckeditor_uploader.fields import RichTextUploadingField


class Banner(Model):
    name = CharField(max_length=100, unique=True, verbose_name='Nombre')
    interval = IntegerField(default=1, verbose_name='Intervalo',
                            help_text='Expresado en milisegundos. 1000 = 1 segundo')

    image_1 = ImageField(upload_to='pages/banners', verbose_name='Imagen 1')
    image_2 = ImageField(
        upload_to='pages/banners', blank=True, null=True, verbose_name='Imagen 2')
    image_3 = ImageField(
        upload_to='pages/banners', blank=True, null=True, verbose_name='Imagen 3')
    image_4 = ImageField(
        upload_to='pages/banners', blank=True, null=True, verbose_name='Imagen 4')
    image_5 = ImageField(
        upload_to='pages/banners', blank=True, null=True, verbose_name='Imagen 5')
    image_6 = ImageField(
        upload_to='pages/banners', blank=True, null=True, verbose_name='Imagen 6')

    def get_images(self) -> list[str]:
        images = []
        for i in range(6):
            image_attr = getattr(self, f'image_{i + 1}')
            if image_attr:
                images.append(image_attr)
        return images

    def __str__(self) -> CharField:
        return self.name


class HomePage(Model):
    class Meta:
        verbose_name = 'Página de inicio'
        verbose_name_plural = 'Página de inicio'

    top_subtitle = CharField(max_length=255, blank=True, null=True, verbose_name='Subtítulo superior')
    title = CharField(max_length=255, blank=True, null=True, verbose_name='Título')
    subtitle = CharField(max_length=255, blank=True, null=True, verbose_name='Subtítulo inferior')
    banner = ForeignKey(Banner, on_delete=PROTECT, blank=True, null=True)
    
    def __str__(self) -> str:
        return 'Home'


class AboutPage(Model):
    class Meta:
        verbose_name = 'Página "Sobre nosotros"'
        verbose_name_plural = 'Página "Sobre nosotros"'

    title = CharField(max_length=255, blank=True, null=True, verbose_name='Título')
    subtitle = CharField(max_length=255, blank=True, null=True, verbose_name='Subtítulo')
    banner = ForeignKey(Banner, on_delete=PROTECT, blank=True, null=True)
    content = TextField(verbose_name='Contenido')

    def __str__(self) -> str:
        return 'Sobre nosotros'


